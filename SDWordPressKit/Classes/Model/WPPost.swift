//
//  Post.swift
//  SDWordPressKit
//
//  Created by Imthath M on 14/04/19.
//  Copyright © 2019 Sky Dev. All rights reserved.
//

import Foundation
import RealmSwift

public class WPPost: Object {
    @objc dynamic var identifier = 0
    @objc dynamic var title = ""
    @objc dynamic var htmlContent: HTML? = HTML()
    @objc dynamic var excerpt: HTML? = HTML()
    @objc dynamic var categoryId = 0
    @objc dynamic var publishedTime = Date()
    @objc dynamic var modifiedTime = Date()

    override public class func primaryKey() -> String? {
        return "identifier"
    }

    convenience init(identifier: Int,
                     categoryId: Int,
                     published: Date,
                     modified: Date,
                     titleString: String,
                     contentString: String,
                     excerptString: String) {

        self.init()
        self.identifier = identifier
        self.categoryId = categoryId
        self.publishedTime = published
        self.modifiedTime = modified
        self.title = titleString

        self.htmlContent = HTML()
        self.excerpt = HTML()

        self.htmlContent?.body = contentString
        self.excerpt?.body = excerptString
    }
}

public class HTML: Object {
    var body = ""

    convenience init(bodyString: String) {
        self.init()
        self.body = bodyString
    }

    var attributedString: NSAttributedString {
        guard let data = body.data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }

    var string: String {
        return attributedString.string
    }

    override public class func ignoredProperties() -> [String] {
        return ["string", "attributedString"]
    }
}
