//
//  Category.swift
//  SDWordPressKit
//
//  Created by Imthath M on 14/04/19.
//  Copyright © 2019 Sky Dev. All rights reserved.
//

import Foundation
import RealmSwift

public class WPCategory: Object {
    @objc dynamic var identifier = 0
    @objc dynamic var postsCount = 0
    @objc dynamic var name = ""
    @objc dynamic var lastSynced = Date(timeIntervalSince1970: EpochTime.jan2018)

    override public class func primaryKey() -> String? {
        return "identifier"
    }

    convenience init(identifier: Int, postsCount: Int, name: String) {
        self.init()
        self.identifier = identifier
        self.postsCount = postsCount
        self.name = name
    }
}
