//
//  Constants.swift
//  SDWordPressKit
//
//  Created by Imthath M on 18/04/19.
//  Copyright © 2019 Sky Dev. All rights reserved.
//

import Foundation

internal let UTC = "yyyy-MM-dd'T'HH:mm:ss"
internal let postsPerPage = 10
internal let baseURLstring = "http://brokencricket.com/wp-json/wp/v2"

internal struct EpochTime {
    static let jan2018: TimeInterval = 1514764800
}
