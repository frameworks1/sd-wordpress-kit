//
//  CommonUtils.swift
//  SDWordPressKit
//
//  Created by Imthath M on 17/04/19.
//  Copyright © 2019 Sky Dev. All rights reserved.
//

import Foundation

extension String {

    /// returns an optinal date if the string is in the specified date format
    ///
    /// - Parameters:
    ///     - format of the date, default is UTC
    func toDate(format: String = UTC) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.date(from: self)
    }
}

extension Date {

    /// returns the string for the date in the specified date format, default is UTC
    func toString(format: String = UTC) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
