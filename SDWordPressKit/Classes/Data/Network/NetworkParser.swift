//
//  NetworkParser.swift
//  SDWordPressKit
//
//  Created by Imthath M on 14/04/19.
//  Copyright © 2019 Sky Dev. All rights reserved.
//

import Foundation
import SwiftyJSON

extension WPPost {
    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case modifiedTimeString = "modified"
        case htmlContent = "content"
        case categoryIdsArray = "categories"
        case title, excerpt
    }
}

extension HTML {
    enum CodingKeys: String, CodingKey {
        case body = "rendered"
    }
}

extension WPCategory {
    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case posts = "count"
        case name
    }
}

internal class Parser {
    internal static func parsePosts(from data: Data) -> [WPPost] {
        var result = [WPPost]()

        guard let jsonList = try? JSON(data: data) else { return result }

        for (_, json) in jsonList {

            guard let identifier = json["id"].int,
                let modified = json["modified"].string,
                let published = json["date"].string,
                let categoryId = json["categories"][0].int,
                let title = json["title"]["rendered"].string,
                let content = json["content"]["rendered"].string,
                let excerpt = json["excerpt"]["rendered"].string,
                let publishedTime = published.toDate(),
                let modifiedTime = modified.toDate() else {
                    continue
            }

            result.append(WPPost(identifier: identifier,
                                 categoryId: categoryId,
                                 published: publishedTime,
                                 modified: modifiedTime,
                                 titleString: title,
                                 contentString: content,
                                 excerptString: excerpt))
        }

        return result
    }

    internal static func parseCategories(from data: Data) -> [WPCategory] {
        var result = [WPCategory]()
        guard let jsonList = try? JSON(data: data) else { return result }

        for (_, json) in jsonList {

            guard let identifier = json["id"].int,
                let name = json["name"].string,
                let posts = json["count"].int else {
                    continue
            }

            result.append(WPCategory(identifier: identifier, postsCount: posts, name: name))
        }

        return result
    }
}
