//
//  NetworkService.swift
//  SDWordPressKit
//
//  Created by Imthath M on 14/04/19.
//  Copyright © 2019 Sky Dev. All rights reserved.
//

//import Foundation
import Moya
import Result
import Alamofire

enum NetworkService {
    case getPosts
    case getCategories
    case getPostsInCategory(id: Int, count: Int?, after: Date?)
}

extension NetworkService: TargetType {
    var baseURL: URL {
        return URL(string: baseURLstring)!
    }

    var path: String {
        switch self {
        case .getPosts, .getPostsInCategory:
            return "/posts"
        case .getCategories:
            return "/categories"
        }
    }

    var method: Alamofire.HTTPMethod {
        return .get
    }

    var sampleData: Data {
        return Data()
    }

    var task: Task {
        switch self {
        case .getPosts, .getCategories:
            return .requestPlain
        case .getPostsInCategory(let categoryId, let count, let date):
            var params = [String: Any]()
            params["categories"] = categoryId
            params["per_page"] = count
            params["after"] = date?.toString()
            return .requestParameters(parameters: params,
                                      encoding: URLEncoding.queryString)
        }
    }

    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }

}

public protocol NetworkContract {
    func getPosts(in category: WPCategory, callback: @escaping ([WPPost]?) -> Void)
    func getCategories(callback: @escaping ([WPCategory]?) -> Void)
}

public class NetworkProvider: NetworkContract {
    let network = MoyaProvider<NetworkService>()
    let decoder = JSONDecoder()

    public func getPosts(in category: WPCategory, callback: @escaping ([WPPost]?) -> Void) {
        network.request(.getPostsInCategory(id: category.identifier, count: postsPerPage,
                                            after: category.lastSynced)) { result in
            switch result {
            case .success(let response):
                let posts = Parser.parsePosts(from: response.data)
                callback(posts)
            case .failure(let error):
                print(error)
                callback(nil)
            }
        }
    }

    public func getCategories(callback: @escaping ([WPCategory]?) -> Void ) {
        network.request(.getCategories) { result in
            switch result {
            case .success(let response):
                let categories = Parser.parseCategories(from: response.data)
                let requiredCategories = categories.filter { $0.name != "Uncategorized" }
                callback(requiredCategories)
            case .failure(let error):
                print(error)
                callback(nil)
            }

        }
    }
}
