//
//  DataManager.swift
//  SDWordPressKit
//
//  Created by Imthath M on 14/04/19.
//  Copyright © 2019 Sky Dev. All rights reserved.
//

import Foundation

public protocol DataMangerContract {
    func getPosts(inCategory categoryName: String) -> [WPPost]
    func getCategoryNames() -> [String]
    func sync(completion: @escaping (Bool) -> Void)
}

public class DataManager: DataMangerContract {
    var database: DatabaseContract = DatabaseService()
    var network: NetworkContract = NetworkProvider()

    public init() { }

    public init(database: DatabaseContract, network: NetworkContract) {
        self.database = database
        self.network = network
    }

    public func sync(completion: @escaping (Bool) -> Void) {
        network.getCategories { categories in
            guard let existingCategories = categories else {
                completion(false)
                return
            }
            self.database.save(existingCategories)
            let syncState = CategoriesSyncState(categories: existingCategories.count)

            existingCategories.forEach { category in
                self.syncPosts(in: category) { isComplete in
                    syncState.checkedCategories += 1

                    if isComplete {
                        self.database.updateSyncedTime(for: category, to: Date())
                        syncState.syncedCategories += 1
                    }

                    if syncState.isCheckComplete {
                        completion(syncState.isSyncComplete)
                    }
                }
            }

        }
    }

    public func getPosts(inCategory categoryName: String) -> [WPPost] {
        return database.getPosts(inCategory: categoryName)
    }

    public func getCategoryNames() -> [String] {
        return database.getCategoryNames()
    }

    private func syncPosts(in category: WPCategory, completion: @escaping (Bool) -> Void) {
        network.getPosts(in: category) { posts in
            if let existingPosts = posts {
                self.database.save(existingPosts)
                completion(true)
            } else {
                completion(false)
            }
        }
    }
}

private class CategoriesSyncState {
    var categoriesCount: Int
    var syncedCategories = 0
    var checkedCategories = 0

    var isCheckComplete: Bool {
        return checkedCategories == categoriesCount
    }

    var isSyncComplete: Bool {
        return syncedCategories == categoriesCount
    }

    fileprivate init(categories count: Int) {
        categoriesCount = count
    }
}
