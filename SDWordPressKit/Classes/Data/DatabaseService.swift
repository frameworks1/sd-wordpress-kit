//
//  DatabaseService.swift
//  SDWordPressKit
//
//  Created by Imthath M on 14/04/19.
//  Copyright © 2019 Sky Dev. All rights reserved.
//

import Foundation
import RealmSwift

public protocol DatabaseContract {
    func migrate()
    func save(_ objects: [Object])
    func getPosts(inCategory categoryName: String) -> [WPPost]
    func getCategoryNames() -> [String]
    func updateSyncedTime(for category: WPCategory, to time: Date)
}

public class DatabaseService: DatabaseContract {
    var schemaVersion: UInt64
    var config: Realm.Configuration

    init() {
        let versionString: String = File.read(from: "schemaVersion") ?? "0"
        schemaVersion = UInt64(versionString)!
        config = Realm.Configuration(schemaVersion: schemaVersion)
    }

    func saveSchemaVersion() {
        try! String(schemaVersion).write(to: File.getUrl(of: "schemaVersion"), atomically: true, encoding: .utf8)
    }

    public func migrate() {
        schemaVersion += 1
        saveSchemaVersion()

        let config = Realm.Configuration(schemaVersion: schemaVersion,
                                         migrationBlock: { _, oldScehmaVersion in
                                            if oldScehmaVersion < self.schemaVersion { }
        })

        _ = try! Realm(configuration: config)
    }

    public func save(_ objects: [Object]) {
        let realm = try! Realm(configuration: config)
        try! realm.write { realm.add(objects, update: true) }
    }

    public func getPosts(inCategory categoryName: String) -> [WPPost] {
        let realm = try! Realm(configuration: config)
        let categoryId = realm.objects(WPCategory.self)
                                .filter("name = '\(categoryName)'").first!.identifier
        return realm.objects(WPPost.self).filter("categoryId = \(categoryId)").toArray()
    }

    public func getCategoryNames() -> [String] {
        let realm = try! Realm(configuration: config)
        return realm.objects(WPCategory.self).toArray().map { $0.name }
    }

    public func updateSyncedTime(for category: WPCategory, to time: Date) {
        let realm = try! Realm(configuration: config)
        try! realm.write {
            category.lastSynced = time
        }
    }
}

public class File {

    static func getUrl(of name: String) -> URL {
        var fileUrl: URL?
        do {
            let docDirectoryUrl = try FileManager.default.url(for: .documentDirectory,
                                                              in: .userDomainMask, appropriateFor: nil, create: true)
            fileUrl = docDirectoryUrl.appendingPathComponent(name).appendingPathExtension("txt")
        } catch let error as NSError {
            print("unable to get file url: \(error.description)")
        }
        return fileUrl!
    }

    static func read(from name: String) -> String? {
        var result: String?
        do {
            result = try String(contentsOf: getUrl(of: name))
        } catch let error as NSError {
            print("unable to read from file at \(getUrl(of: name)) \nError\(error.description)")
        }
        return result
    }

}

extension Results {

    func toArray() -> [Element] {
        return self.map {$0}
    }
}

public enum Category: String {
    case features = "Features"
    case news = "News"
    case social = "Social"
    case stats = "Stats"
}
