Pod::Spec.new do |spec|
 
  spec.name         = "SDWordPressKit"
  spec.version      = "0.1.0"
  spec.summary      = "Swift Handler for posts and categories from word press website. Supports all Apple platforms."

  spec.description  = <<-DESC
  Swift Handler for posts and categories from word press website. Supports all Apple platforms.
                   DESC

  spec.homepage     = "https://gitlab.com/imthath_m/skydevpodspecs.git"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author             = { "Imthath M" => "imthath.m@icloud.com" }
  spec.social_media_url   = "https://twitter.com/imthath_m"
  spec.source       = { :git => "https://gitlab.com/imthath_m/sd-wordpress-kit.git", :tag => "#{spec.version}" }
  spec.swift_version = "4.2"
  spec.osx.deployment_target = "10.12"
  spec.ios.deployment_target = "9.0"
  spec.watchos.deployment_target = "2.0"
  spec.tvos.deployment_target = "9.0"
  
  spec.source_files  = "UtilitiesExample/Utils/*.swift"


end
