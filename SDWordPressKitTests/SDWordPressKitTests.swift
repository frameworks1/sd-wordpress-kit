//
//  SDWordPressKitTests.swift
//  SDWordPressKitTests
//
//  Created by Imthath M on 14/04/19.
//  Copyright © 2019 Sky Dev. All rights reserved.
//

import XCTest
@testable import SDWordPressKit

class SDWordPressKitTests: XCTestCase {
    let dataManager = DataManager()

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testGetCaegories() {
        dataManager.getCategoryNames().forEach { print($0) }
    }
    
    func testGetPosts() {
        dataManager.getPosts(inCategory: "Features").forEach { print($0) }
    }
    
    func testSync() {
        let exp = expectation(description: "waiting to sync ")
        dataManager.sync() { isComplete in
            XCTAssert(isComplete)
            exp.fulfill()
        }
        waitForExpectations(timeout: 150, handler: nil)
    }
    
    func testMigrate() {
        let database = DatabaseService()
        database.migrate()
    }
    
    func testNetwork() {
        let exp = expectation(description: "waiting for network")
        let network = NetworkProvider()
        let category = WPCategory(identifier: 111, postsCount: 36, name: "News")
        network.getPosts(in: category) { posts in
            exp.fulfill()
            print(posts?.count)
        }
        waitForExpectations(timeout: 100, handler: nil)
    }

}
